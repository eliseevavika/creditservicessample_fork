package Services;

import Domain.Bank;
import Domain.CreditRequestModel;
import Domain.CreditType;

import java.util.ArrayList;

/**
 * Created by promazol on 15.11.2016.
 */
public interface ICreditCalculatorService {
    //расчет из модели
    ArrayList<Double> Calculate(CreditRequestModel model);
    //расчет по параметрам
    ArrayList<Double> CalculateByParams(int paymentType, double months, double percent, double amount);
    //высчет процента
    double GetPercent(Bank bank, CreditType creditType);
}
