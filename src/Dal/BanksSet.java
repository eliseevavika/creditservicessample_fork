package Dal;

import Domain.Bank;
import Domain.BankToCreditTypeRel;
import Domain.CreditType;

import java.util.*;

/**
 * Created by promazol on 15.11.2016.
 */
public class BanksSet extends DataSet<Bank> {
    private static BanksSet ourInstance = new BanksSet();
    public static BanksSet getInstance() {
        return ourInstance;
    }

    @Override
    protected void FillData() {
        CreditTypeSet creditTypeSet = CreditTypeSet.getInstance();

        this.Data.add(new Bank(1, "ТрансКапиталБанк", new BankToCreditTypeRel[]{
                new BankToCreditTypeRel(creditTypeSet.GetById(1), 0.21),
                new BankToCreditTypeRel(creditTypeSet.GetById(2), 0.155),
                new BankToCreditTypeRel(creditTypeSet.GetById(3), 0.14),
                new BankToCreditTypeRel(creditTypeSet.GetById(4), 0.14),
                new BankToCreditTypeRel(creditTypeSet.GetById(5), 0.185)
        }));
        this.Data.add(new Bank(2, "АбсолютБанк", new BankToCreditTypeRel[]{
                new BankToCreditTypeRel(creditTypeSet.GetById(1), 0.165),
                new BankToCreditTypeRel(creditTypeSet.GetById(2), 0.1475),
                new BankToCreditTypeRel(creditTypeSet.GetById(3), 0.12),
                new BankToCreditTypeRel(creditTypeSet.GetById(4), 0.12),
                new BankToCreditTypeRel(creditTypeSet.GetById(5), 0.1475)
        }));
        this.Data.add(new Bank(3, "УралСибБанк", new BankToCreditTypeRel[]{
                new BankToCreditTypeRel(creditTypeSet.GetById(1), 0.19),
                new BankToCreditTypeRel(creditTypeSet.GetById(2), 0.19),
                new BankToCreditTypeRel(creditTypeSet.GetById(3), 0.125),
                new BankToCreditTypeRel(creditTypeSet.GetById(4), 0.1125),
                new BankToCreditTypeRel(creditTypeSet.GetById(5), 0.1525)
        }));
    }

    public String GetHelp(){
        String bankHelp = "Bведите номер банка: ";

        for (Iterator<Bank> i = this.Data.iterator(); i.hasNext();) {
            Bank bank = i.next();
            bankHelp += bank.Id + " - " + bank.Name;
            if (i.hasNext()) {
                bankHelp += ", ";
            } else {
                bankHelp += ": ";
            }
        }

        return bankHelp;
    }
}