package Dal;

import Domain.PaymentType;

import java.util.Iterator;

/**
 * Created by promazol on 15.11.2016.
 */
public class PaymentTypeSet extends DataSet<PaymentType> {
    private static PaymentTypeSet ourInstance = new PaymentTypeSet();
    public static PaymentTypeSet getInstance() {
        return ourInstance;
    }

    @Override
    protected void FillData() {
        this.Data.add(new PaymentType(1, "Аннуитетные платежи"));
        this.Data.add(new PaymentType(2, "Дифференцированные платежи"));
    }

    public String GetHelp(){
        String bankHelp = "Выбирайте вид выплаты кредита: ";

        for (Iterator<PaymentType> i = this.Data.iterator(); i.hasNext();) {
            PaymentType bank = i.next();
            bankHelp += bank.Id + " - " + bank.Name;
            if (i.hasNext()) {
                bankHelp += ", ";
            } else {
                bankHelp += ": ";
            }
        }

        return bankHelp;
    }
}