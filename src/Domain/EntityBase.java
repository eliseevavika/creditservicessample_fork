package Domain;

/**
 * Created by promazol on 15.11.2016.
 */
public class EntityBase {
    public int Id;
    public String Name;

    public EntityBase(int id, String name) {
        Id = id;
        Name = name;
    }
}